package com.json.jsonexample;

public class DataModel {

    public String getBoard() {
        return board;
    }

    public String getId() {
        return id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public DataModel(String board, String id, String thumbnail) {
        this.board = board;
        this.id = id;
        this.thumbnail = thumbnail;
    }

    private final String board;
    private final String id;
    private final String thumbnail;

}
